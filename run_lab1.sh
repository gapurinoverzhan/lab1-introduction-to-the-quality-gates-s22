sudo kill $(ps aux | grep java | grep -v 'grep' | awk '{print $2}')
sudo apt update
sudo apt install docker.io -y
sudo docker login -u "$DOCKER_LOGIN" -p "$DOCKER_TOKEN" docker.io
sudo docker rmi "$LAB1_IMAGE"
sudo docker run -p 8080:8080 -d "$LAB1_IMAGE"